import {
  LOAD_USERS_DATA
} from '../types'


const defaultState = {
  friendsGraph: {
    nodes: [],
    graph: [],
    edges: [],
  },
}


export default (state = defaultState, action) => {
  switch (action.type) {
    case LOAD_USERS_DATA:
      return {
        friendsGraph: action.payload
      }
    default:
      return state
  }
}

