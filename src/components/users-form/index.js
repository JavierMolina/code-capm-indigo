import React from 'react'
import axios from 'axios'
import { connect } from 'react-redux'
import { Formik, Form, Field, ErrorMessage } from 'formik'
import { API_SERVER } from '../../types'
import { setUsersData } from './actions'


const onSubmit = async (props, values, { setSubmitting }) => {
  setSubmitting(false)
  try {
    const response = await axios.get(
      `${API_SERVER}/api/twitter/GetFriendsGraph?screenName1=${values.user1}&screenName2=${values.user2}`
    )
    props.setUsersData(response.data)
  } catch (error) {
    console.error(error);
  }
}


const UsersForm = props => <React.Fragment>
  <h1>Find your friends in common</h1>
  <div>
    <Formik
      initialValues={{ user1: '', user2: '' }}
      validate={values => {
        let errors = {}
        if (!values.user1) {
          errors.user1 = 'Required'
        }
        if (!values.user1) {
          errors.user2 = 'Required'
        }
        return errors
      }}
      onSubmit={(values, setSubmitting) => onSubmit(props, values, setSubmitting)}
    >
      {({ isSubmitting }) => (
        <Form>
          <Field type="text" name="user1" />
          <ErrorMessage name="user1" component="div" />
          <Field type="text" name="user2" />
          <ErrorMessage name="user2" component="div" />
          <button type="submit" disabled={isSubmitting}>
            Submit
          </button>
        </Form>
      )}
    </Formik>
  </div>
</React.Fragment>

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch => {
  return {
    setUsersData: value => {
      dispatch(setUsersData(value))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersForm)
