import {
  LOAD_USERS_DATA,
} from '../types'

export const setUsersData = value => ({
  type: LOAD_USERS_DATA,
  payload: value
})