import React, { Component } from 'react'
import { connect } from 'react-redux'
import Graph from 'vis-react'


const options = {
  layout: {
    hierarchical: true
  },
  edges: {
    color: "#000000"
  }
}


const events = {
  select: (event) => {
    var { nodes, edges } = event
  }
}

const renderGraph = props => {
  const edges = getEdges(props)
  const nodes = getNodes(props)
  const graph = { nodes, edges }
  return <Graph graph={graph} options={options} events={events} />
}

const divStyle = {
  width: '800px',
  height: '800px',
  margin: '40px',
  border: '5px solid blue'
};

const getEdges = props => props.friendsGraph.edges
const getNodes = props => props.friendsGraph.nodes


const Grafos = props => <React.Fragment>
  <h1>Graph</h1>
  <div style={divStyle}>
    {renderGraph(props)}
  </div>
</React.Fragment>

const mapStateToProps = state => {
  return {
    friendsGraph: state.info.friendsGraph
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // setUsersData: value => {
    //   dispatch(setUsersData(value))
    // },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Grafos)