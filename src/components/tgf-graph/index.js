import React, { Component } from 'react'
import { connect } from 'react-redux'


const renderNodes = props => {
  return props.friendsGraph.nodes.map(row => <p>{row.id} - {row.label}</p>)
}

const renderGraphText = props => {
  return props.friendsGraph.graph.map(row => <p>{row}</p>)
}

const TGFGraph = props => <React.Fragment>
  <h1>Nodes</h1>
  {renderNodes(props)}
  {props.friendsGraph.nodes.length > 0 ? <p>#</p> : ''}
  {renderGraphText(props)}
</React.Fragment>

const mapStateToProps = state => {
  return {
    friendsGraph: state.info.friendsGraph
  }
}

const mapDispatchToProps = dispatch => {
  return {
    // setUsersData: value => {
    //   dispatch(setUsersData(value))
    // },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TGFGraph)