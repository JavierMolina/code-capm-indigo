import React from 'react'
import { Provider } from 'react-redux'
import configureStore from './redux/store'
import './App.css'
import UsersForm from './components/users-form'
import TGFGraph from './components/tgf-graph'
import Graph from './components/graph'

const store = configureStore()

function App() {
  return (
    <Provider store={store}>
      <UsersForm />
      <TGFGraph />
      <Graph />
    </Provider>
  )
}

export default App
